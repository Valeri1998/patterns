final class EventManager {
    private var listeners = [ListenerProtocol]()
    
    func add(listener: ListenerProtocol) {
        if !listeners.contains(where: { $0 === listener }) {
            listeners.append(listener)
        }
    }
    
    func remove(listener: ListenerProtocol) {
        if listeners.contains(where: { $0 === listener }) {
            listeners.removeAll(where: { $0 === listener })
        }
    }
    
    func notifyListenersWillSetSomeValue(newValue: String) {
        listeners.forEach({ $0.willSetSomeValue(newValue: newValue) })
    }
    
    func notifyListenersDidSetSomeValue(oldValue: String) {
        listeners.forEach({ $0.didSetSomeValue(oldValue: oldValue) })
    }
}

final class EventCreator {
    private let eventManager = EventManager()
    var someValue: String = "" {
        willSet(nv) {
            eventManager.notifyListenersWillSetSomeValue(newValue: nv)
        }
        didSet(ov) {
            eventManager.notifyListenersDidSetSomeValue(oldValue: ov)
        }
    }
    
    func add(listener: ListenerProtocol) {
        eventManager.add(listener: listener)
    }
    
    func remove(listener: ListenerProtocol) {
        eventManager.remove(listener: listener)
    }
}


protocol ListenerProtocol: AnyObject {
    func willSetSomeValue(newValue: String)
    func didSetSomeValue(oldValue: String)
}

class BaseListener: ListenerProtocol {
    var listenerName: String {
        return String(describing: type(of: self))
    }
    
    func willSetSomeValue(newValue: String) {
        print("\(listenerName), \(#function). Value: \(newValue)")
    }
    
    func didSetSomeValue(oldValue: String) {
        print("\(listenerName), \(#function). Value: \(oldValue)")
    }
}

final class Listener1: BaseListener {
}

final class Listener2: BaseListener {
}

final class Listener3: BaseListener {
}


let creator = EventCreator()

let listener1 = Listener1()
let listener2 = Listener2()
let listener3 = Listener3()

creator.add(listener: listener1)
creator.add(listener: listener2)
creator.add(listener: listener3)

creator.someValue = "1"
creator.someValue = "2"
creator.remove(listener: listener1)
creator.remove(listener: listener2)
creator.someValue = "3"
