enum ComponentEvent {
    case action1
    case action2
    case action3
    case action4
    case action5
}

protocol MediatorProtocol: AnyObject {
    func notify(_ sender: BaseComponent, event: ComponentEvent)
}


final class ConcreteMediator: MediatorProtocol {
    private let component1: Component1
    private let component2: Component2
    
    init(component1: Component1, component2: Component2) {
        self.component1 = component1
        self.component2 = component2
        
        self.component1.setup(mediator: self)
        self.component2.setup(mediator: self)
    }
    
    func notify(_ sender: BaseComponent, event: ComponentEvent) {
        switch event {
        case .action1:
            component1.action2()
        case .action2:
            component2.action3()
        case .action3:
            component2.action4()
        case .action4:
            component2.action5()
        case .action5:
            print("There will be an eternal cycle if: component1.action1()")
        }
    }
}


class BaseComponent {
    var mediator: MediatorProtocol?
    
    func setup(mediator: MediatorProtocol) {
        self.mediator = mediator
    }
}

class Component1: BaseComponent {
    func action1() {
        print("Component 1 did .action1")
        mediator?.notify(self, event: .action1)
    }
    
    func action2() {
        print("Component 1 did .action2")
        mediator?.notify(self, event: .action2)
    }
}

class Component2: BaseComponent {
    func action3() {
        print("Component 2 did .action3")
        mediator?.notify(self, event: .action3)
    }
    
    func action4() {
        print("Component 2 did .action4")
        mediator?.notify(self, event: .action4)
    }
    
    func action5() {
        print("Component 2 did .action5")
        mediator?.notify(self, event: .action5)
    }
}


let component1 = Component1()
let component2 = Component2()
let mediator = ConcreteMediator(component1: component1, component2: component2)

component1.action1()
