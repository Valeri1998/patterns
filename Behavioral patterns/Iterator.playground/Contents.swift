import UIKit

final class SomeCollection: Sequence {
    private var items = [String]()
    
    subscript (_ index: Int) -> String? {
        guard 0 <= index && index < items.endIndex else {
            return nil
        }
        
        return items[index]
    }
    
    func append(_ item: String) {
        items.append(item)
    }
    
    func makeIterator() -> some IteratorProtocol {
        return CollectionIterator(collection: self)
    }
}

final class CollectionIterator: IteratorProtocol {
    private let collection: SomeCollection
    private var index = 0
    
    init(collection: SomeCollection) {
        self.collection = collection
    }
    
    func next() -> String? {
        defer {
            index += 1
        }
        
        return collection[index]
    }
}



func iterateCollectionItems<T: Sequence>(collection: T) {
    var collectionIterator = collection.makeIterator()

    while let nextItem = collectionIterator.next() {
        print(nextItem)
    }
}

let someCollection = SomeCollection()
someCollection.append("FirstCollection. Item 1")
someCollection.append("FirstCollection. Item 2")
someCollection.append("FirstCollection. Item 3")
someCollection.append("FirstCollection. Item 4")
someCollection.append("FirstCollection. Item 5")
iterateCollectionItems(collection: someCollection)
