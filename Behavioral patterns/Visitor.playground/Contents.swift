protocol ComponentProtocol: AnyObject {
    func accept(visitor: VisitorProtocol)
}

final class Component1: ComponentProtocol {
    func accept(visitor: VisitorProtocol) {
        visitor.methodForComponent1(self)
    }
}

final class Component2: ComponentProtocol {
    func accept(visitor: VisitorProtocol) {
        visitor.methodForComponent2(self)
    }
}

final class Component3: ComponentProtocol {
    func accept(visitor: VisitorProtocol) {
        visitor.methodForComponent3(self)
    }
}


protocol VisitorProtocol: AnyObject {
    func methodForComponent1(_ component: Component1)
    func methodForComponent2(_ component: Component2)
    func methodForComponent3(_ component: Component3)
}

class BaseVisitor: VisitorProtocol {
    private var visitorName: String {
        return String(describing: type(of: self))
    }
    
    func methodForComponent1(_ component: Component1) {
        print("\(visitorName), concrete realization \(#function)")
    }
    
    func methodForComponent2(_ component: Component2) {
        print("\(visitorName), concrete realization \(#function)")
    }
    
    func methodForComponent3(_ component: Component3) {
        print("\(visitorName), concrete realization \(#function)")
    }
}

final class ConcreteVisitor1: BaseVisitor {
}

final class ConcreteVisitor2: BaseVisitor {
}


let components: [ComponentProtocol] = [Component1(), Component2(), Component3()]
let visitors = [ConcreteVisitor1(), ConcreteVisitor2()]

visitors.forEach { visitor in
    components.forEach { component in
        component.accept(visitor: visitor)
    }
}
