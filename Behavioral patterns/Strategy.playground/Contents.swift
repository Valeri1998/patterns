protocol StrategyProtocol: AnyObject {
    func method1()
}

class BaseStrategy: StrategyProtocol {
    private var strategyName: String {
        return String(describing: type(of: self))
    }
    
    func method1() {
        print("\(strategyName), \(#function)")
    }
}

final class ConcreteStrategy1: BaseStrategy {
}

final class ConcreteStrategy2: BaseStrategy {
}


final class Context {
    private var strategy: StrategyProtocol?
    
    func setup(strategy: StrategyProtocol) {
        self.strategy = strategy
    }
    
    func executeAction() {
        strategy?.method1()
    }
}


let strategy1 = ConcreteStrategy1()
let strategy2 = ConcreteStrategy2()

let context = Context()
context.setup(strategy: strategy1)
context.executeAction()

context.setup(strategy: strategy2)
context.executeAction()
