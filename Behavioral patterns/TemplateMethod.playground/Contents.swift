import UIKit

protocol AbstractProtocol: AnyObject {
    func templateMethod()
    
    func operation1()
    func operation2()
    func operation3()
    
    func requiredOperation1()
    func requiredOperation2()
}

extension AbstractProtocol {
    var className: String {
        return String(describing: type(of: self))
    }
    
    func requiredOperation1() {
        print("\(className), \(#function)")
    }
    
    func requiredOperation2() {
        print("\(className), \(#function)")
    }
}

class BaseAbstractMethodClass: AbstractProtocol {
    final func templateMethod() {
        operation1()
        requiredOperation1()
        operation2()
        requiredOperation2()
        operation3()
    }
    
    func operation1() {
        //
    }
    
    func operation2() {
        //
    }
    
    func operation3() {
        //
    }
}


final class Realization1: BaseAbstractMethodClass {
    override func operation1() {
        print("\(className), \(#function)")
    }
    
    override func operation2() {
        print("\(className), \(#function)")
    }
    
    override func operation3() {
        print("\(className), \(#function)")
    }
}

final class Realization2: BaseAbstractMethodClass {
    override func operation2() {
        print("\(className), \(#function)")
    }
}


let realizations = [Realization1(), Realization2()]
realizations.forEach({
    $0.templateMethod()
    print("---")
})
