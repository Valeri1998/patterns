//Адекватный пример
protocol CommandProtocol: AnyObject {
    func execute()
}

class DefaultCommand: CommandProtocol {
    let reciver: CommandReceiver
    
    init(reciver: CommandReceiver) {
        self.reciver = reciver
    }
    
    func execute() {
        assertionFailure("BaseCommand. execute() must be overriden")
    }
}

class ConcreteCommand1: DefaultCommand {
    override func execute() {
        reciver.methodForConcreteCommand1()
    }
}

class ConcreteCommand2: DefaultCommand {
    override func execute() {
        reciver.methodForConcreteCommand1()
    }
}


final class CommandReceiver {
    func methodForConcreteCommand1() {
        print("CommandReceiver. methodForConcreteCommand1 executed")
    }
    
    func methodForConcreteCommand2() {
        print("CommandReceiver. methodForConcreteCommand2 executed")
    }
}


//В качестве отправителя команды выступит сам playground
let reciver = CommandReceiver()

let commend1 = ConcreteCommand1(reciver: reciver)
let commend2 = ConcreteCommand2(reciver: reciver)

let commands: [CommandProtocol] = [ConcreteCommand1(reciver: reciver),  ConcreteCommand2(reciver: reciver)]
commands.forEach({ $0.execute() })










//Ниже странновантый пример из книжки
final class CommandHistory {
    private var commands = [Command]()
    
    func push(command: Command) {
        commands.append(command)
    }
    
    func pop() -> Command? {
        guard !commands.isEmpty else {
            return nil
        }
        
        return commands.removeLast()
    }
}

final class Editor {
    var text: String?
    
    func getSelection() -> String? {
        print("Editor. Selected text received")
        return "Selected text"
    }
    
    func deleteSelection() {
        print("Editor. Selection is deleted")
    }
    
    func replaceSelection(text: String?) {
        self.text = text
        
        print("Editor. Selection is replaced")
    }
}


protocol Command: AnyObject {
    @discardableResult
    func execute() -> Bool
    
    func saveBackup()
    func undo()
}
    
class BaseCommand: Command {
    let editor: Editor
    let application: AppCommand
    
    private var backup: String?
    
    init(app: AppCommand, editor: Editor) {
        application = app
        self.editor = editor
    }
    
    func saveBackup() {
        backup = editor.text
    }
    
    func undo() {
        editor.text = backup
    }
    
    @discardableResult
    func execute() -> Bool {
        assertionFailure("BaseCommand. execute() must be overriden")
        return true
    }
}

class CopyCommand: BaseCommand {
    override func execute() -> Bool {
        application.clipboard = editor.getSelection()
        
        print("CopyCommand is completed")
        return false
    }
}

class CutCommand: BaseCommand {
    override func execute() -> Bool {
        saveBackup()
        application.clipboard = editor.getSelection()
        editor.deleteSelection()
        
        print("CutCommand is completed")
        return true
    }
}

class PasteCommand: BaseCommand {
    override func execute() -> Bool {
        saveBackup()
        editor.replaceSelection(text: application.clipboard)
        
        print("PasteCommand is completed")
        return true
    }
}

class UndoCommand: BaseCommand {
    override func execute() -> Bool {
        application.undo()
        
        print("UndoCommand is completed")
        return false
    }
}

    
protocol AppCommand: AnyObject {
    var clipboard: String? { get set }
    func undo()
}

final class Application: AppCommand {
    let commandHistory = CommandHistory()
    
    var clipboard: String?
    
    func execute(command: Command) {
        if command.execute() {
            commandHistory.push(command: command)
        }
    }
    
    func undo() {
        let lastCommand = commandHistory.pop()
        lastCommand?.undo()
    }
}


let editor = Editor()
let app = Application()

let cutCommand = CutCommand(app: app, editor: editor)
let copyCommand = CopyCommand(app: app, editor: editor)
let pasteCommand = PasteCommand(app: app, editor: editor)
let undoCommand = UndoCommand(app: app, editor: editor)

app.execute(command: cutCommand)
app.execute(command: copyCommand)
app.execute(command: pasteCommand)

app.execute(command: undoCommand)
app.execute(command: undoCommand)

