protocol HandlerProtocol: AnyObject {
    @discardableResult
    func setNext(handler: HandlerProtocol) -> HandlerProtocol
    func handle(request: String)
}

class BaseHandler: HandlerProtocol {
    private(set) var nextHandler: HandlerProtocol?
    
    func setNext(handler: HandlerProtocol) -> HandlerProtocol {
        nextHandler = handler
        return handler
    }
    
    func handle(request: String) {
        print("BaseHandler. func handle(request: String)")
        nextHandler?.handle(request: request)
    }
}

class FirstHandler: BaseHandler {
    override func handle(request: String) {
        print("FirstHandler. func handle(request: String)")
        nextHandler?.handle(request: request)
    }
}

class SecondHandler: BaseHandler {
    override func handle(request: String) {
        print("SecondHandler. func handle(request: String)")
        
        if request == "testRequest" {
            print("SecondHandler finished processing request")
        } else {
            nextHandler?.handle(request: request)
        }
    }
}

class ThridHandler: BaseHandler {
    override func handle(request: String) {
        print("ThridHandler. func handle(request: String)")
        nextHandler?.handle(request: request)
    }
}


let handler1 = FirstHandler()
let handler2 = SecondHandler()
let handler3 = ThridHandler()
let handler4 = BaseHandler()

handler1.setNext(handler: handler2).setNext(handler: handler3).setNext(handler: handler4)
handler1.handle(request: "")
print()
handler1.handle(request: "testRequest")
