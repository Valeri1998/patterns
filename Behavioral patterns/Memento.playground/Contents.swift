protocol MementoProtocol {
    var value1: String { get }
    var value2: String { get }
    var value3: String { get }
}

struct Memento: MementoProtocol {
    private(set) var value1: String
    private(set) var value2: String
    private(set) var value3: String
    
    init(value1: String, value2: String, value3: String) {
        self.value1 = value1
        self.value2 = value2
        self.value3 = value3
    }
}


protocol OriginatorProtocol: AnyObject {
    func createMemento() -> MementoProtocol
    func restoreState(from memento: MementoProtocol)
    
    //Для более удобного тестирования
    func printState()
    func changeState()
}

final class ConcreteOriginator: OriginatorProtocol {
    private var value1: String
    private var value2: String
    private var value3: String
    
    init(value1: String, value2: String, value3: String) {
        self.value1 = value1
        self.value2 = value2
        self.value3 = value3
    }
    
    func changeState() {
        let charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        value1 = String((0..<6).map{ _ in (charSet.randomElement() ?? "_") })
        value2 = String((0..<6).map{ _ in (charSet.randomElement() ?? "_") })
        value3 = String((0..<6).map{ _ in (charSet.randomElement() ?? "_") })
    }
    
    func createMemento() -> MementoProtocol {
        return Memento(value1: value1, value2: value2, value3: value3)
    }
    
    func restoreState(from memento: MementoProtocol) {
        guard let memento = memento as? Memento else {
            return
        }
        
        value1 = memento.value1
        value2 = memento.value2
        value3 = memento.value3
    }
    
    func printState() {
        print("value1: \(value1)\nvalue2: \(value2)\nvalue3: \(value3)")
    }
}

final class Caretaker {
    private let originator: OriginatorProtocol
    private lazy var mementos = [MementoProtocol]()
    
    private var caretakerName: String {
        return String(describing: type(of: self))
    }
    
    init(originator: OriginatorProtocol) {
        self.originator = originator
    }
    
    func backup() {
        print("\(caretakerName). \(#function)")
        let memento = originator.createMemento()
        mementos.append(memento)
    }
    
    func undo() {
        print("\(caretakerName). \(#function)")
        guard let memento = mementos.popLast() else {
            return
        }
        
        originator.restoreState(from: memento)
    }
    
    func showHistory() {
        print("\(caretakerName). \(#function)")
        
        mementos.forEach({
            print("value1: \($0.value1)\nvalue2: \($0.value2)\nvalue3: \($0.value3)")
            print("------------------")
        })
    }

    func showOrigintorState() {
        print("\(caretakerName). \(#function)")
        originator.printState()
    }
    
    func changeOrigintorState() {
        print("\(caretakerName). \(#function)")
        originator.changeState()
    }
}


let originator = ConcreteOriginator(value1: "o1v1", value2: "o1v2", value3: "o1v3")
let caretaker = Caretaker(originator: originator)
caretaker.backup()
caretaker.changeOrigintorState()
caretaker.backup()
caretaker.changeOrigintorState()
caretaker.backup()
caretaker.changeOrigintorState()

caretaker.showHistory()
caretaker.showOrigintorState()

caretaker.undo()
caretaker.showOrigintorState()
caretaker.undo()
caretaker.showOrigintorState()
caretaker.showHistory()
