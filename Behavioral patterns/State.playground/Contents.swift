enum StateType {
    case state1
    case state2
    case state3
}

protocol StateProtocol: AnyObject {
    func setup(context: ContextProtocol)
    
    func method1()
    func method2()
    func method3()
}

class BaseState: StateProtocol {
    private(set) weak var context: ContextProtocol?
    var stateName: String {
        return String(describing: type(of: self))
    }
    
    func setup(context: ContextProtocol) {
        self.context = context
    }
    
    func method1() {
        print("\(stateName), \(#function)")
    }
    
    func method2() {
        print("\(stateName), \(#function)")
    }
    
    func method3() {
        print("\(stateName), \(#function)")
    }
}

final class State1: BaseState {
    override func method1() {
        super.method1()
        context?.changeState(.state2)
    }
}

final class State2: BaseState {
    override func method2() {
        super.method2()
        context?.changeState(.state3)
    }
}

final class State3: BaseState {
    override func method3() {
        super.method3()
        context?.changeState(.state1)
    }
}


protocol ContextProtocol: AnyObject {
    func changeState(_ stateType: StateType)
}

final class Context: ContextProtocol {
    private let state1 = State1()
    private let state2 = State2()
    private let state3 = State3()
    
    private var state: StateProtocol?
    
    func changeState(_ stateType: StateType) {
        switch stateType {
        case .state1:
            state = state1
        case .state2:
            state = state2
        case .state3:
            state = state3
        }
        
        self.state?.setup(context: self)
    }
    
    func performAction1() {
        state?.method1()
    }
    
    func performAction2() {
        state?.method2()
    }
    
    func performAction3() {
        state?.method3()
    }
}


let state = State1()

let context = Context()
context.changeState(.state1)

context.performAction1()
context.performAction2()
context.performAction3()
