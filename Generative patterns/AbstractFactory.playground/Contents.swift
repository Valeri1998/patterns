protocol ProductA {
    var productAName: String { get }
}

protocol ProductB {
    var productBName: String { get }
}

class CertainProductA: ProductA {
    var productAName: String
    
    init(productName: String) {
        productAName = productName
    }
}

class CertainProductB: ProductB {
    var productBName: String
    
    init(productName: String) {
        productBName = productName
    }
}


protocol AbsctractFactoryProtocol {
    func createProductA() -> ProductA
    func createProductB() -> ProductB
}

class CertainFactory1: AbsctractFactoryProtocol {
    func createProductA() -> ProductA {
        return CertainProductA(productName: "My name is \"CertainProductA from CertainFactory1\"")
    }
    
    func createProductB() -> ProductB {
        return CertainProductB(productName: "My name is \"CertainProductB from CertainFactory1\"")
    }
}

class CertainFactory2: AbsctractFactoryProtocol {
    func createProductA() -> ProductA {
        return CertainProductA(productName: "My name is \"CertainProductA from CertainFactory2\"")
    }
    
    func createProductB() -> ProductB {
        return CertainProductB(productName: "My name is \"CertainProductB from CertainFactory2\"")
    }
}


let factories: [AbsctractFactoryProtocol] = [CertainFactory1(), CertainFactory2()]
factories.forEach {
    let productA = $0.createProductA()
    let productB = $0.createProductB()

    print(productA.productAName)
    print(productB.productBName)
    print()
}
