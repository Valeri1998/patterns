protocol FoodBuilderProtocol: AnyObject {
    var food: [String] { get }
    
    func reset()
    func appendCake()
    func appendSauce()
    func appendMeat()
    func appendCheese()
}

class PizzaBuilder: FoodBuilderProtocol {
    private(set) var food = [String]()
    
    func reset() {
        food.removeAll()
    }
    
    func appendCake() {
        food.append("Cake")
    }
    
    func appendSauce() {
        food.append("Sauce")
    }
    
    func appendMeat() {
        food.append("Meat")
    }
    
    func appendCheese() {
        food.append("Cheese")
    }
}


class FoodDirector {
    private var builder: FoodBuilderProtocol?
    
    func setupBuilder(builder: FoodBuilderProtocol?) {
        self.builder = builder
    }
    
    func makePizza() {
        builder?.reset()
        builder?.appendCake()
        builder?.appendSauce()
        builder?.appendMeat()
        builder?.appendMeat()
        builder?.appendCheese()
        builder?.appendCheese()
    }
    
    func makeVeganPizza() {
        builder?.reset()
    }
}


let director = FoodDirector()

let pizzaBuilder = PizzaBuilder()

director.setupBuilder(builder: pizzaBuilder)
director.makePizza()

print("My pizza:")
print(pizzaBuilder.food)

director.makeVeganPizza()

print("My vegan pizza:")
print(pizzaBuilder.food)
