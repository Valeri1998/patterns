import Foundation

protocol Clonable: AnyObject {
    func clone() -> AnyObject
}

class BaseClass: Clonable {
    private(set) var value1: String
    var value2: String
    var value3: String
    
    init(value2: String, value3: String) {
        self.value1 = value2 + value3
        self.value2 = value2
        self.value3 = value3
    }
    
    required init() {
        self.value1 = ""
        self.value2 = ""
        self.value3 = ""
    }
    
    func clone() -> AnyObject {
        let clonedObject = type(of: self).init()
        
        clonedObject.value1 = value1
        clonedObject.value2 = value2
        clonedObject.value3 = value3
        
        return clonedObject
    }
}

class ChildClass: BaseClass {
    private(set) var value4: String
    
    init(value4: String) {
        self.value4 = value4
        
        super.init(value2: value4, value3: value4)
    }
    
    required init() {
        self.value4 = ""
        
        super.init()
    }
    
    override func clone() -> AnyObject {
        guard let clonedObject = super.clone() as? ChildClass else {
            return ChildClass()
        }
        
        clonedObject.value4 = value4
        
        return clonedObject
    }
}

let firstClass = ChildClass(value4: "v4")
let secondClass = firstClass.clone() as! ChildClass
secondClass.value2 = "999"

print("№1. value1: \(firstClass.value1), value2: \(firstClass.value2) value3: \(firstClass.value3), value4: \(firstClass.value4)")
print("№2. value1: \(secondClass.value1), value2: \(secondClass.value2) value3: \(secondClass.value3), value4: \(secondClass.value4)")



class ParentClass: NSCopying {
    private var value1: String
    var value2: String
    
    init(value1: String, value2: String) {
        self.value1 = value1
        self.value2 = value2
    }
    
    required init() {
        self.value1 = ""
        self.value2 = ""
    }
        
    func copy(with zone: NSZone? = nil) -> Any {
        let prototype = type(of: self).init()
        
        prototype.value1 = self.value1
        prototype.value2 = self.value2
        
        return prototype
    }
}

class ChildrenClass: ParentClass {
    var value3: String
    
    init(value3: String) {
        self.value3 = value3
        
        super.init(value1: value3, value2: value3)
    }
    
    required init() {
        value3 = ""
        
        super.init()
    }
    
    override func copy(with zone: NSZone? = nil) -> Any {
        guard let copiedObject = super.copy(with: zone) as? ChildrenClass else {
            return ChildrenClass()
        }
        
        copiedObject.value3 = self.value3
        
        return copiedObject
    }
}

let firstObject = ChildrenClass(value3: "111")
let secondObject = firstObject.copy() as! ChildrenClass
secondObject.value3 = "222"

print("№1. value2: \(firstObject.value2) value3: \(firstObject.value3)")
print("№2. value2: \(secondObject.value2) value3: \(secondObject.value3)")
