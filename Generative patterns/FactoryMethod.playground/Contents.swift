import UIKit

protocol BaseCreator {
    func createEntity() -> Entity
}

extension BaseCreator {
    func createEntity() -> Entity {
        return CommonEntity()
    }
}

class ConcreteCreator1: BaseCreator {
    func createEntity() -> Entity {
        return ConcreteEntity1()
    }
}

class ConcreteCreator2: BaseCreator {
    func createEntity() -> Entity {
        return ConcreteEntity2()
    }
}


protocol Entity {
    init()
}

class CommonEntity: Entity {
    required init() {
        print("I'am CommonEntity")
    }
}

class ConcreteEntity1: Entity {
    required init() {
        print("I'am ConcreteEntity1")
    }
}

class ConcreteEntity2: Entity {
    required init() {
        print("I'am ConcreteEntity2")
    }
}


let creators: [BaseCreator] = [ConcreteCreator1(), ConcreteCreator2()]
creators.forEach({ $0.createEntity() })
