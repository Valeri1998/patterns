class Singleton {
    static let sharedInstance = Singleton()
    private init() {}
    
    var className: String {
        return String(describing: type(of: self))
    }
}


print(Singleton.sharedInstance.className)
