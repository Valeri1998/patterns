class DetailNameService {
    var name: String {
        return "Name"
    }
    
    var surname: String {
        return "Surname"
    }
    
    var patronymic: String {
        return "Patronymic"
    }
    
    var intAge: Int {
        return 1488
    }
}

class CommonNameServiceAdapter {
    private let detailNameService = DetailNameService()
    
    var fullName: String {
        return "\(detailNameService.name) \(detailNameService.surname) \(detailNameService.patronymic)"
    }
    
    var strintAge: String {
        return String(detailNameService.intAge)
    }
}


let nameService = CommonNameServiceAdapter()

let fullName: String = nameService.fullName
let age: String = nameService.strintAge

print("Name: \(fullName)\nAge: \(age)")
