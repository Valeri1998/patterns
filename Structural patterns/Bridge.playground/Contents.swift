protocol RealizationProtocol {
    func printCommand1()
    func printCommand2()
    func printCommand3()
    func printCommand4()
    func printCommand5()
}

class BaseRealization: RealizationProtocol {
    func printCommand1() {
        print("BaseRealization. Command 1")
    }
    
    func printCommand2() {
        print("BaseRealization. Command 2")
    }
    
    func printCommand3() {
        print("BaseRealization. Command 3")
    }
    
    func printCommand4() {
        print("BaseRealization. Command 4")
    }
    
    func printCommand5() {
        print("BaseRealization. Command 5")
    }
}

class Realization1: BaseRealization {
    override func printCommand1() {
        print("Realization1. Command 1")
    }
    
    override func printCommand2() {
        print("Realization1. Command 2")
    }
    
    override func printCommand3() {
        print("Realization1. Command 3")
    }
    
    override func printCommand4() {
        print("Realization1. Command 4")
    }
    
    override func printCommand5() {
        print("Realization1. Command 5")
    }
}

class Realization2: BaseRealization {
    override func printCommand1() {
        print("Realization2. Command 1")
    }
    
    override func printCommand2() {
        print("Realization2. Command 2")
    }
    
    override func printCommand3() {
        print("Realization2. Command 3")
    }
    
    override func printCommand4() {
        print("Realization2. Command 4")
    }
    
    override func printCommand5() {
        print("Realization2. Command 5")
    }
}
    
class Abstraction {
    private let realization: RealizationProtocol
    
    init(realization: RealizationProtocol) {
        self.realization = realization
    }
    
    func callAllCommands() {
        realization.printCommand1()
        realization.printCommand2()
        realization.printCommand3()
        realization.printCommand4()
        realization.printCommand5()
    }
    
    func callFirstCommand() {
        realization.printCommand1()
    }
}


let abstraction1 = Abstraction(realization: Realization1())
abstraction1.callAllCommands()
abstraction1.callFirstCommand()


let abstraction2 = Abstraction(realization: Realization2())
abstraction2.callAllCommands()
abstraction2.callFirstCommand()
