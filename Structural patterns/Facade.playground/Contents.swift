protocol SubsystemProtocol {
    func subsystemMethod1()
    func subsystemMethod2()
    func subsystemMethod3()
}

final class Subsystem1: SubsystemProtocol {
    func subsystemMethod1() {
        print("Subsystem1. subsystemMethod1")
    }
    
    func subsystemMethod2() {
        print("Subsystem1. subsystemMethod2")
    }
    
    func subsystemMethod3() {
        print("Subsystem1. subsystemMethod3")
    }
}

final class Subsystem2: SubsystemProtocol {
    func subsystemMethod1() {
        print("Subsystem2. subsystemMethod1")
    }
    
    func subsystemMethod2() {
        print("Subsystem2. subsystemMethod2")
    }
    
    func subsystemMethod3() {
        print("Subsystem2. subsystemMethod3")
    }
}


class Facade {
    private let subsystems: [SubsystemProtocol] = {
        return [Subsystem1(), Subsystem2()]
    }()
    
    func makeSystemMethod1() {
        subsystems.forEach({ $0.subsystemMethod1() })
    }
    
    func makeSystemMethod2() {
        subsystems.forEach({ $0.subsystemMethod2() })
    }
    
    func makeSystemMethod3() {
        subsystems.forEach({ $0.subsystemMethod3() })
    }
}


let facade = Facade()

facade.makeSystemMethod1()
facade.makeSystemMethod2()
facade.makeSystemMethod3()
