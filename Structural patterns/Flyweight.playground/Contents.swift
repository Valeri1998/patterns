import CoreGraphics

enum TextureType: String {
    case type1
    case type2
    case type3
}

struct TreeType {
    let name: String
    let hexColor: String
    let textureType: TextureType
}

struct Tree {
    let treeType: TreeType
    let xPosition: CGFloat
    let yPosition: CGFloat
    
    func drawTree() {
        print("Three x: \(xPosition) y: \(yPosition) name: \(treeType.name) hexColor: \(treeType.hexColor) textureType: \(treeType.textureType)")
    }
}

class TreeFactory {
    private var treeTypes = [TreeType]()
    static let sharedInstance = TreeFactory()
    
    private init() {}
    
    func getTreeType(by name: String, hexColor: String, textureType: TextureType) -> TreeType {
        if let treeType = treeTypes.first(where: { $0.name == name && $0.hexColor == hexColor && $0.textureType == textureType }) {
            
            print("Type found")
            return treeType
        } else {
            let treeType = TreeType(name: name, hexColor: hexColor, textureType: textureType)
            treeTypes.append(treeType)
            
            print("Type not found")
            return treeType
        }
    }
}

class FleweightForest {
    private var trees = [Tree]()
    
    func createTree(x: CGFloat, y: CGFloat, name: String, hexColor: String, textureType: TextureType) {
        let treeType = TreeFactory.sharedInstance.getTreeType(by: name, hexColor: hexColor, textureType: textureType)
        let tree = Tree(treeType: treeType, xPosition: x, yPosition: y)
        
        trees.append(tree)
    }
    
    func createForest() {
        trees.forEach({ $0.drawTree() })
    }
}



let forest = FleweightForest()
forest.createTree(x: 0.0, y: 0.0, name: "Древо 1", hexColor: "666", textureType: .type1)
forest.createTree(x: 1.0, y: 3.0, name: "Древо 2", hexColor: "666", textureType: .type2)
forest.createTree(x: 2.0, y: 4.0, name: "Древо 3", hexColor: "666", textureType: .type3)

forest.createTree(x: 0.0, y: 0.0, name: "Древо 1", hexColor: "666", textureType: .type1)
forest.createTree(x: 1.0, y: 3.0, name: "Древо 2", hexColor: "666", textureType: .type2)
forest.createTree(x: 2.0, y: 4.0, name: "Древо 3", hexColor: "666", textureType: .type3)

forest.createForest()
