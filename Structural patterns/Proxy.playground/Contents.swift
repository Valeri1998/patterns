protocol ServiceInterface: AnyObject {
    func someMethod1()
    func someMethod2()
}

final class Service: ServiceInterface {
    func someMethod1() {
        print("Service. someMethod1")
    }
    
    func someMethod2() {
        print("Service. someMethod2")
    }
}

final class ServiceProxy: ServiceInterface {
    private let realService: ServiceInterface
    
    init() {
        realService = Service()
    }
        
    func someMethod1() {
        print("ServiceProxy. someMethod1")
        realService.someMethod1()
    }
    
    func someMethod2() {
        print("ServiceProxy. someMethod2")
        realService.someMethod2()
    }
}


func workWithService(service: ServiceInterface) {
    service.someMethod1()
    service.someMethod2()
    print()
}


let realService: ServiceInterface = Service()
let serviceProxy: ServiceInterface = ServiceProxy()

workWithService(service: realService)
workWithService(service: serviceProxy)
