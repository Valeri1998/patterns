protocol ComponentInterface {
    func componentOperation()
}

final class ConcreteComponent: ComponentInterface {
    func componentOperation() {
        print("ConcreteComponent componentOperation")
    }
}

class FirstDecorator: ComponentInterface {
    let decoratedObject: ComponentInterface
    
    init(decoratedObject: ComponentInterface) {
        self.decoratedObject = decoratedObject
    }
    
    func componentOperation() {
        decoratedObject.componentOperation()
    }
    
    func firstDecoratorCustomMethod() {
        print("FirstDecorator firstDecoratorCustomMethod")
    }
}

class SecondDecorator: FirstDecorator {
    override func firstDecoratorCustomMethod() {
        super.firstDecoratorCustomMethod()
        print("SecondDecorator overriden firstDecoratorCustomMethod")
    }
    
    func secondDecoratorCustomMethod() {
        print("SecondDecorator firstDecoratorCustomMethod")
    }
}

let component = ConcreteComponent()
let firstDecorator = FirstDecorator(decoratedObject: component)
let secondDecorator = SecondDecorator(decoratedObject: firstDecorator)

secondDecorator.componentOperation()
secondDecorator.firstDecoratorCustomMethod()
secondDecorator.secondDecoratorCustomMethod()
