protocol Component: AnyObject {
    func performSomeOperation()
}

class Leaf: Component {
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func performSomeOperation() {
        print("Leaf name: \(name)")
    }
}

class ChildLeaf: Leaf {
    override func performSomeOperation() {
        print("Child leaf name: \(name)")
    }
}

class Branch: Component {
    private var leafs = [Component]()
    
    func append(component: Component) {
        leafs.append(component)
    }
    
    func remove(component: Component) {
        leafs.removeAll(where: { $0 === component })
    }
    
    func performSomeOperation() {
        leafs.forEach({ $0.performSomeOperation() })
    }
}



let branch1 = Branch()
let leaf1ForBranch1 = Leaf(name: "leaf1ForBranch1")
let leaf2ForBranch1 = Leaf(name: "leaf2ForBranch1")
let leaf3ForBranch1 = Leaf(name: "leaf3ForBranch1")

branch1.append(component: leaf1ForBranch1)
branch1.append(component: leaf2ForBranch1)
branch1.append(component: leaf3ForBranch1)

let branch2 = Branch()
let leaf1ForBranch2 = Leaf(name: "leaf1ForBranch2")
let leaf2ForBranch2 = Leaf(name: "leaf2ForBranch2")

branch2.append(component: leaf1ForBranch2)
branch2.append(component: leaf2ForBranch2)

let branch3 = Branch()
let leaf1ForBranch3 = Leaf(name: "leaf1ForBranch3")
let leaf2ForBranch3 = ChildLeaf(name: "childLeaf2ForBranch3")
let leaf3ForBranch3 = Leaf(name: "leaf3ForBranch3")

branch3.append(component: leaf1ForBranch3)
branch3.append(component: leaf2ForBranch3)
branch3.append(component: leaf3ForBranch3)

let three = Branch()
three.append(component: branch1)
three.append(component: branch2)
three.append(component: branch3)

three.performSomeOperation()
